package com.fortegroup.fortetv20.own.network;


import com.fortegroup.fortetv20.own.network.model.ActivationInfo;
import com.fortegroup.fortetv20.own.network.model.DeviceInfo;
import com.fortegroup.fortetv20.own.network.model.Playlist;

import retrofit.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by etb on 23.05.16.
 */
public interface ForteAPI {

    @POST("screen/registerScreen")
    public Observable<Integer> register(@Body DeviceInfo info);

    @GET("playlist/getPlaylistForScreen/{playlistID}")
    public Observable<Playlist> getPlaylist(@Path(value = "playlistID")Integer playlistId);

    @POST("screen/activateScreen")
    public Observable<Response<Void>> activate(@Body ActivationInfo info);
}

package com.fortegroup.fortetv20.own.network;

import android.app.Activity;

import com.fortegroup.fortetv20.BuildConfig;
import com.fortegroup.fortetv20.own.application.ForteApp;
import com.fortegroup.fortetv20.own.network.DeviceInfoService;
import com.fortegroup.fortetv20.own.network.ForteServerService;
import com.fortegroup.fortetv20.own.ui.ConfigurationDialog;
import com.fortegroup.fortetv20.own.utils.SharedPrefHelper;

/**
 * Created by etb on 26.05.16.
 */
public enum DI {

    MODULE;

    private static abstract class Lazy<T>{

        private T instance;

        protected abstract T init();

        public T get(){
            if(instance == null){
                instance = init();
            }

            return instance;
        }

    }

    {
        server = new Lazy<ForteServerService>() {
            @Override
            protected ForteServerService init() {
                return getRealService();
            }
        };

        deviceInfo = new Lazy<DeviceInfoService>() {
            @Override
            protected DeviceInfoService init() {
                return new DeviceInfoService(getCurrentActivity());
            }
        };

    }

    private Lazy<ForteServerService> server;
    private Lazy<DeviceInfoService> deviceInfo;

    public ForteServerService getForteService(){
        return server.get();
    }

    private ForteServerService getTestService(){
        return new ForteServerService(new TestForteAPI());
    }

    private Activity getCurrentActivity(){
        return ForteApp.getCurrentActivity();
    }

    private ForteServerService getRealService(){
        return new ForteServerService(SharedPrefHelper.getString(ConfigurationDialog.HOST_KEY, null));
    }

    public DeviceInfoService getDeviceInfoService(){
        return deviceInfo.get();
    }
}

package com.fortegroup.fortetv20.own.ui;

import com.fortegroup.fortetv20.own.MediaControllerListener;

/**
 * Created by etb on 23.05.16.
 */
public interface MediaControllerObservable {

    public void setListener(MediaControllerListener listener);
}

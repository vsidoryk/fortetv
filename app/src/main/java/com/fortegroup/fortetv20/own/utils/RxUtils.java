package com.fortegroup.fortetv20.own.utils;

import android.util.Log;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by etb on 26.05.16.
 */
public class RxUtils {

    public static <T> Observable<T> inPool(Observable<T> toWrap){
        return toWrap.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io());
    }

    public static <T> Observer<T> getLogObserver(final String tag){
        return new Observer<T>(){

            @Override
            public void onCompleted() {
                Log.d(tag, "onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                Log.e(tag, String.format("onError %s", e.getCause()));
            }

            @Override
            public void onNext(T t) {
                Log.d(tag, "onNext " + t);
            }
        };
    }
}

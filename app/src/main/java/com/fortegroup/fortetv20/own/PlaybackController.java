package com.fortegroup.fortetv20.own;

import android.app.Fragment;
import android.os.Handler;
import android.os.Looper;

import com.fortegroup.fortetv20.own.network.model.Playlist;
import com.fortegroup.fortetv20.own.network.model.PlaylistRow;
import com.fortegroup.fortetv20.own.network.model.RowType;
import com.fortegroup.fortetv20.own.ui.ImageFragment;
import com.fortegroup.fortetv20.own.ui.MainActivity;
import com.fortegroup.fortetv20.own.ui.MediaControllerObservable;
import com.fortegroup.fortetv20.own.ui.VideoFragment;

import java.util.Iterator;
import java.util.List;


/**
 * Created by etb on 23.05.16.
 */
public class PlaybackController implements MediaControllerListener {

    private static final long DEFAULT_IMAGE_DURATION = 1000;

    private static final long PLAY_ITEM_COMPLETELY = -1;

    private List<PlaylistRow> playlist;

    private Iterator<PlaylistRow> iterator;
    private Routing routing;

    private int currentPosition;
    private Handler scheduler;

    public PlaybackController(Routing routing, Playlist playlist){
        this.routing = routing;
        this.playlist = playlist.getMediaList();
        this.iterator = playlist.getMediaList().iterator();
        this.scheduler = new Handler(Looper.getMainLooper());
    }

    private Runnable timerCallback = new Runnable() {
        @Override
        public void run() {
            if(!iterator.hasNext()){
                iterator = playlist.iterator();
            }

            play();

        }
    };

    private Long getDefaultDurationFor(RowType type){
        switch (type){
            case IMAGE:
                return DEFAULT_IMAGE_DURATION;
            default:
                return PLAY_ITEM_COMPLETELY;

        }
    }

    public void play(){
        PlaylistRow current = iterator.next();
        prepareScreen(current);
        long duration = current.getDuration();
        if(duration == 0){
            duration = getDefaultDurationFor(current.getType());
        }

        if(duration != PLAY_ITEM_COMPLETELY) {
            scheduler.postDelayed(timerCallback, duration);
        }
    }

    private void prepareScreen(PlaylistRow row){

        switch (row.getType()){
            case IMAGE:
                Router.getInstance(routing).toImage(row.getUrl());
                break;
            case MOVIE:
                Router.getInstance(routing).toVideo(row.getUrl());
                break;
            default:
                new IllegalStateException();
        }

    }

    public void pause(){
        scheduler.removeCallbacks(timerCallback);
    }


    @Override
    public void onMediaStop() {
        scheduler.removeCallbacks(timerCallback);
        timerCallback.run();
    }

    @Override
    public void onMediaPause() {

    }

    @Override
    public void onMediaPlay() {

    }
}

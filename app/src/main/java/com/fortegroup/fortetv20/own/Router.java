package com.fortegroup.fortetv20.own;

import com.fortegroup.fortetv20.own.ui.EmptyPlaylistFragment;
import com.fortegroup.fortetv20.own.ui.ImageFragment;
import com.fortegroup.fortetv20.own.ui.LoadingFragment;
import com.fortegroup.fortetv20.own.ui.VideoFragment;

import java.lang.ref.WeakReference;

/**
 * Created by etb on 25.05.16.
 */
public class Router {

    public static Router getInstance(Routing routing){
        if(sInstance == null){
            sInstance = new Router(routing);
        } else if(sInstance.routingRef.get() != routing){
            sInstance = new Router(routing);
        }

        return sInstance;

    }

    private static Router sInstance;

    private WeakReference<Routing> routingRef;

    private Router(Routing routing){
        this.routingRef = new WeakReference<>(routing);
    }

    private Routing ensureRouting(){
        Routing routing = routingRef.get();
        if(routing == null){
            throw new IllegalStateException();
        }

        return routing;
    }

    public void toVideo(String url){
        ensureRouting().replaceFragment(VideoFragment.newInstance(url));
    }

    public void toImage(String url){
        ensureRouting().replaceFragment(ImageFragment.newInstance(url));
    }

    public void loading(){
        ensureRouting().replaceFragment(new LoadingFragment());
    }

    public void emptyPlaylist(){
        ensureRouting().replaceFragment(new EmptyPlaylistFragment());
    }

}

package com.fortegroup.fortetv20.own.network;

import com.fortegroup.fortetv20.own.application.ForteApp;
import com.fortegroup.fortetv20.own.network.model.ActivationInfo;
import com.fortegroup.fortetv20.own.network.model.DeviceInfo;
import com.fortegroup.fortetv20.own.network.model.Playlist;

import retrofit.Response;
import retrofit.http.Body;
import retrofit.http.Path;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by etb on 25.05.16.
 */
public class TestForteAPI implements ForteAPI{


    @Override
    public Observable<Integer> register(@Body DeviceInfo info) {
        return Observable.create(new Observable.OnSubscribe<Integer>(){

            @Override
            public void call(Subscriber<? super Integer> subscriber) {
                try {
                    Thread.sleep(2000);
                    testUpdate();
                    subscriber.onNext(6);
                    subscriber.onCompleted();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    private void testUpdate(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(7000);
                    ForteApp.getCurrentActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DI.MODULE.getForteService().update();
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    @Override
    public Observable<Playlist> getPlaylist(@Path(value = "playlistID") Integer playlistId) {
        return Observable.create(new Observable.OnSubscribe<Playlist>() {
            @Override
            public void call(Subscriber<? super Playlist> subscriber) {
                try{
                    Thread.sleep(4000);
                    subscriber.onNext(Playlist.mockedFromJson());
                    subscriber.onCompleted();
                } catch (InterruptedException e){
                    subscriber.onError(e);
                }
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Response<Void>> activate(@Body ActivationInfo info) {
        return Observable.create(new Observable.OnSubscribe<Response<Void>>() {
            @Override
            public void call(Subscriber<? super Response<Void>> subscriber) {
                try{
                    Thread.sleep(1000);
                   // subscriber.onNext(Response.success(null));
                    subscriber.onCompleted();
                } catch (InterruptedException e){
                    subscriber.onError(e);
                }
            }
        });

    }
}

package com.fortegroup.fortetv20.own.network;

import android.app.Activity;
import android.util.Log;

import com.fortegroup.fortetv20.own.network.model.ActivationInfo;
import com.fortegroup.fortetv20.own.network.model.DeviceInfo;
import com.fortegroup.fortetv20.own.network.model.Playlist;
import com.fortegroup.fortetv20.own.utils.RxUtils;
import com.fortegroup.fortetv20.own.utils.SharedPrefHelper;

import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.subjects.BehaviorSubject;

/**
 * Created by etb on 23.05.16.
 */
public class ForteServerService {

    private static final String TAG = ForteServerService.class.getCanonicalName();

    private static final String PLAYLIST_ID = ForteServerService.class.getCanonicalName() + ".Playlist";

    ForteServerService(String url){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api = retrofit.create(ForteAPI.class);
    }

    ForteServerService(ForteAPI api){
        this.api = api;
    }

    private ForteAPI api;
    private BehaviorSubject<Integer> innerObservable;

    private void register(){
        Log.d(TAG, "register");
        DI.MODULE.getDeviceInfoService().getObservableValue()
                .flatMap(new Func1<DeviceInfo, Observable<Integer>>() {
            @Override
            public Observable<Integer> call(DeviceInfo deviceInfo) {
                return RxUtils.inPool(api.register(deviceInfo));
            }
        }).doOnNext(new Action1<Integer>() {
            @Override
            public void call(Integer integer) {
                SharedPrefHelper.getEditor().putInt(PLAYLIST_ID, integer).commit();
                getInnerObservable().onNext(integer);
            }
        }).subscribe(RxUtils.getLogObserver(TAG));
    }

    private void activate(final int id){
        Log.d(TAG, "activate");
        DI.MODULE.getDeviceInfoService().getObservableValue()
                .flatMap(new Func1<DeviceInfo, Observable<Response<Void>>>() {
                    @Override
                    public Observable<Response<Void>> call(DeviceInfo info) {
                        return RxUtils.inPool(api.activate(new ActivationInfo(String.valueOf(id), info.googleId)));
                    }
                }).doOnNext(new Action1<Response<Void>>() {
            @Override
            public void call(Response<Void> aVoid) {
                getInnerObservable().onNext(id);
            }
        }).subscribe(RxUtils.getLogObserver(TAG));
    }


    private BehaviorSubject<Integer> getInnerObservable(){
        if(innerObservable == null){
            innerObservable = BehaviorSubject.create();
        }

        return innerObservable;
    }

    public Observable<Playlist> getPlaylistObservable(){
        if(innerObservable == null){
            Integer playlistID = SharedPrefHelper.getInt(PLAYLIST_ID, -1);
            if(playlistID == -1) {
                register();
            } else {
                activate(playlistID);
            }
        }

        return getInnerObservable().flatMap(new Func1<Integer, Observable<Playlist>>() {
            @Override
            public Observable<Playlist> call(Integer integer) {
                return RxUtils.inPool(api.getPlaylist(integer));
            }
        });
    }

    public void update(){
        Log.d(TAG, "update");
        getInnerObservable().onNext(innerObservable.getValue());
    }

}

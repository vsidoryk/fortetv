package com.fortegroup.fortetv20.own.network.model;

import com.fortegroup.fortetv20.own.TestJsons;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by etb on 23.05.16.
 */
public class Playlist {

    public static Playlist mocked(PlaylistRow[] rows){
        Playlist result = new Playlist();
        result.mediaList = new ArrayList();
        result.mediaList.addAll(Arrays.asList(rows));
        return result;
    }

    public static Playlist mockedFromJson(){
        return new Gson().fromJson(TestJsons.PLAYLIST, Playlist.class);
    }



    private boolean isSorted;
    List<PlaylistRow> mediaList;

    public boolean isEmpty(){
        return mediaList == null || mediaList.isEmpty();
    }

    public List<PlaylistRow> getMediaList(){
        if(!isSorted){
            Collections.sort(mediaList, PlaylistRow.comparator());
            isSorted = true;
        }

        return mediaList;
    }

}



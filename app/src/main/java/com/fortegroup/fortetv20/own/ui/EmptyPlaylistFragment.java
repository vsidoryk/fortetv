package com.fortegroup.fortetv20.own.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.fortegroup.fortetv20.R;
import com.fortegroup.fortetv20.own.Router;
import com.fortegroup.fortetv20.own.Routing;
import com.fortegroup.fortetv20.own.network.DI;
import com.fortegroup.fortetv20.own.network.ForteServerService;

/**
 * Created by etb on 25.05.16.
 */
public class EmptyPlaylistFragment extends Fragment {

    private Button button;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_empty, null, false);
        inflateViews(view);
        return view;
    }

    private void inflateViews(View view){
        button = (Button) view.findViewById(R.id.refreshButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Router.getInstance((Routing) getActivity()).loading();
                DI.MODULE.getForteService().update();
            }
        });
    }


}

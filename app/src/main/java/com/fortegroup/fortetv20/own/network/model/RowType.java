package com.fortegroup.fortetv20.own.network.model;

/**
 * Created by etb on 24.05.16.
 */
public enum RowType {

    IMAGE("PNG", "JPG", "GIF"),
    MOVIE("AVI", "MP4", "MKV"),
    AUDIO("MP3");

    private String[] extensions;

    RowType( String ... extensions){
        this.extensions = extensions;
    }

    public static RowType find(String extension){
        for(RowType type : values()){
            for(String typeExt : type.extensions){
                if(typeExt.equalsIgnoreCase(extension)){
                    return type;
                }
            }
        }

        throw new IllegalArgumentException(String.format("Can't find type for this extension %s", extension));
    }
}

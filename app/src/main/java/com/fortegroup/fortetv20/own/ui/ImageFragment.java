package com.fortegroup.fortetv20.own.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v17.leanback.app.BackgroundManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.fortegroup.fortetv20.R;
import com.fortegroup.fortetv20.own.MediaControllerListener;

/**
 * Created by etb on 23.05.16.
 */
public class ImageFragment extends Fragment implements MediaControllerObservable {
    private static final String IMAGE_URL_KEY = VideoFragment.class.getSimpleName() + ".ImageUrl";

    public static ImageFragment newInstance(String videoUrl){
        ImageFragment result = new ImageFragment();
        Bundle args = new Bundle();
        args.putString(IMAGE_URL_KEY, videoUrl);
        result.setArguments(args);

        return result;
    }

    private ImageView image;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, null, false);
        injectViews(view);
        processArg();
        return view;
    }



    private void processArg() {
        String url = getArguments().getString(IMAGE_URL_KEY);

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        final BackgroundManager backgroundManager = BackgroundManager.getInstance(getActivity());
        backgroundManager.attach(getActivity().getWindow());

        Glide.with(getActivity())
                .load(url)
                .centerCrop()
                .into(new SimpleTarget<GlideDrawable>(metrics.widthPixels, metrics.heightPixels) {
                    @Override
                    public void onResourceReady(GlideDrawable resource,
                                                GlideAnimation<? super GlideDrawable> glideAnimation) {
                        backgroundManager.setDrawable(resource);
                    }
                });

    }

    private void injectViews(View view) {
        image = (ImageView) view.findViewById(R.id.imageView);

    }


    @Override
    public void setListener(MediaControllerListener listener) {

    }
}

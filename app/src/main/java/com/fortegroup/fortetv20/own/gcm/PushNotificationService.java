package com.fortegroup.fortetv20.own.gcm;

import android.os.Bundle;
import android.util.Log;

import com.fortegroup.fortetv20.own.network.DI;
import com.fortegroup.fortetv20.own.network.ForteServerService;
import com.google.android.gms.gcm.GcmListenerService;

/**
 * Created by etb on 23.05.16.
 */
public class PushNotificationService extends GcmListenerService {

    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.d(PushNotificationService.class.getCanonicalName(), "onMessage");
        DI.MODULE.getForteService().update();
    }
}

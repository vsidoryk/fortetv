package com.fortegroup.fortetv20.own.application;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;

import java.lang.ref.WeakReference;

/**
 * Created by etb on 23.05.16.
 */
public class ForteApp extends MultiDexApplication{

    private static WeakReference<Activity> activityRef;
    private static WeakReference<Context> contextRef;

    @Override
    public void onCreate() {
        super.onCreate();

        contextRef = new WeakReference<Context>(getApplicationContext());
        registerActivityLifecycleCallbacks(new EmptyActivityLifecycleCallbacks(){
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                super.onActivityCreated(activity, savedInstanceState);
                activityRef = new WeakReference<>(activity);
            }
        });
    }

    public static Activity getCurrentActivity(){
        if(activityRef == null){
            return null;
        }

        return activityRef.get();
    }

    public static Context getContext(){
        if(contextRef == null){
            return null;
        }

        return contextRef.get();
    }
}

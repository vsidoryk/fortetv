package com.fortegroup.fortetv20.own.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.fortegroup.fortetv20.own.application.ForteApp;

/**
 * Created by etb on 23.05.16.
 */
public class SharedPrefHelper {
    private static final String PREF_FILE_NAME = "defaultPrefFile";

    //
    public static SharedPreferences getDefaultPref() {
        return ForteApp.getContext().getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }


    //
    public static SharedPreferences.Editor getEditor() {
        return getDefaultPref().edit();
    }

    //
    public static void storeLong(String key, long value) {
        getEditor().putLong(key, value).commit();
    }

    public static int getInt(String key, Integer value){
        return getDefaultPref().getInt(key, value);
    }

    //
    public static Long getLong(String key, long defValue) {
        return getDefaultPref().getLong(key, defValue);
    }

    //
    public static void storeString(String key, String value) {
        getEditor().putString(key, value).commit();
    }

    //
    public static String getString(String key, String defValue) {
        return getDefaultPref().getString(key, defValue);
    }
}

package com.fortegroup.fortetv20.own.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.fortegroup.fortetv20.BuildConfig;
import com.fortegroup.fortetv20.R;
import com.fortegroup.fortetv20.own.utils.SharedPrefHelper;

/**
 * Created by etb on 23.05.16.
 */
public class ConfigurationDialog extends DialogFragment implements DialogInterface.OnClickListener {

    public static final String HOST_KEY = ConfigurationDialog.class.getSimpleName() + ".Host";

    public interface NameListener{
        void onNameReceived(String name);
    }

    private EditText nameET;
    private EditText hostET;

    private NameListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_configuration, null);
        builder.setView(view);

        nameET = (EditText)view.findViewById(R.id.editName);
        hostET = (EditText) view.findViewById(R.id.editHost);

        builder.setTitle(R.string.configuration_title);
        builder.setPositiveButton(R.string.next, this);

        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        String name = nameET.getEditableText().toString();
        String host = hostET.getEditableText().toString();

        if(TextUtils.isEmpty(name) || TextUtils.isEmpty(host)){
            return;
        }

        SharedPrefHelper.getEditor()
                .putString(HOST_KEY, host + BuildConfig.FORTE_SERVER_PATH)
                .commit();

        if(listener != null){
            listener.onNameReceived(name);
        }

        this.dismiss();

    }

    public void setNameObserver(NameListener action){
        this.listener = action;
    }
}

package com.fortegroup.fortetv20.own.network.model;

import java.util.Comparator;

/**
 * Created by etb on 23.05.16.
 */
public class PlaylistRow {


    public static PlaylistRow mocked(int position, String url, long duration){
        PlaylistRow result = new PlaylistRow();
        result.sequenceNumber = position;
        result.filename = url;
        result.duration = duration;

        return result;
    }

    public static Comparator<PlaylistRow> comparator(){
        return new Comparator<PlaylistRow>() {
            @Override
            public int compare(PlaylistRow lhs, PlaylistRow rhs) {
                int leftPosition = lhs.sequenceNumber;
                int rightPosition = rhs.sequenceNumber;

                if(leftPosition < rightPosition){
                    return -1;
                } else if(rightPosition > leftPosition){
                    return 1;
                } else {
                    return 0;
                }

            }
        };
    }

    int sequenceNumber;

    String filename;
    long duration;
    RowType type;

    public String getUrl(){
        return filename;
    }

    public RowType getType(){
        if(type == null){
            String[] parts = filename.split("\\.");
            type = RowType.find(parts[parts.length - 1]);
        }

        return type;
    }

    public long getDuration(){
        return duration;
    }

}

package com.fortegroup.fortetv20.own.network.model;

/**
 * Created by etb on 26.05.16.
 */
public class ActivationInfo {
    public final String id;
    public final String googleId;

    public ActivationInfo(String id, String googleId){
        this.id = id;
        this.googleId = googleId;
    }
}

package com.fortegroup.fortetv20.own.network.model;

import android.text.TextUtils;

import com.fortegroup.fortetv20.own.utils.SharedPrefHelper;

/**
 * Created by etb on 23.05.16.
 */
public class DeviceInfo {

    public static final String NAME_KEY = DeviceInfo.class.getSimpleName() + ".Name";
    public static final String ID_KEY = DeviceInfo.class.getSimpleName() + ".ID";

    public static DeviceInfo saved(){
        String name = SharedPrefHelper.getString(NAME_KEY, null);
        String id = SharedPrefHelper.getString(ID_KEY, null);
        if(TextUtils.isEmpty(name) || TextUtils.isEmpty(id)){
            throw new IllegalStateException();
        }

        return new DeviceInfo(name, id);

    }

    public final String name;
    public final String googleId;

    public DeviceInfo(String deviceName, String deviceId){
        this.googleId = deviceId;
        this.name = deviceName;
    }

    public void save(){
        SharedPrefHelper.getEditor()
                .putString(NAME_KEY, name)
                .putString(ID_KEY, googleId)
                .commit();

    }
}

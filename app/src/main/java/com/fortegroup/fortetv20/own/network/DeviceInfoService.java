package com.fortegroup.fortetv20.own.network;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import com.fortegroup.fortetv20.BuildConfig;
import com.fortegroup.fortetv20.own.gcm.GCMClientManager;
import com.fortegroup.fortetv20.own.network.model.DeviceInfo;
import com.fortegroup.fortetv20.own.ui.ConfigurationDialog;
import com.fortegroup.fortetv20.own.utils.SharedPrefHelper;

import java.lang.ref.WeakReference;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func2;
import rx.subjects.BehaviorSubject;

/**
 * Created by etb on 23.05.16.
 */
public class DeviceInfoService {

    private BehaviorSubject<String> nameSubject = BehaviorSubject.create();
    private BehaviorSubject<String> idSubject = BehaviorSubject.create();

    private BehaviorSubject<DeviceInfo> innerObservable;

    private WeakReference<Activity> activityRef;

    DeviceInfoService(Activity activity){
        this.activityRef = new WeakReference<>(activity);
    }

    public Observable<DeviceInfo> getObservableValue(){
        getDeviceName();
        registerGcm();

        return getInnerObservable().asObservable();
    }

    private BehaviorSubject<DeviceInfo> getInnerObservable(){
        if(innerObservable == null){
            innerObservable = BehaviorSubject.create();
            Observable.zip(nameSubject.asObservable(), idSubject.asObservable(), new Func2<String, String, DeviceInfo>() {
                @Override
                public DeviceInfo call(String s, String s2) {
                    return new DeviceInfo(s, s2);
                }
            }).subscribe(new Action1<DeviceInfo>() {
                @Override
                public void call(DeviceInfo deviceInfo) {
                    innerObservable.onNext(deviceInfo);
                }
            });
        }

        return innerObservable;
    }


    private void getDeviceName(){
        String name = SharedPrefHelper.getString(DeviceInfo.NAME_KEY, null);

        if(!TextUtils.isEmpty(name)){
            nameSubject.onNext(name);
        } else {
            showAlert(activityRef.get());
        }

    }

    private void showAlert(Activity activity){
        ConfigurationDialog dialog = new ConfigurationDialog();
        dialog.setNameObserver(new ConfigurationDialog.NameListener() {
            @Override
            public void onNameReceived(String name) {
                SharedPrefHelper.getEditor().putString(DeviceInfo.NAME_KEY, name).commit();
                nameSubject.onNext(name);
            }
        });
        dialog.show(activity.getFragmentManager(), "");
    }

    private void registerGcm(){
        GCMClientManager pushClientManager = new GCMClientManager(activityRef.get(), BuildConfig.PROJECT_ID);
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {
                Log.d("Registration id", registrationId);
                idSubject.onNext(registrationId);
            }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
                getInnerObservable().onError(new Throwable(ex));
            }
        });

    }
}

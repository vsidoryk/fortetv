package com.fortegroup.fortetv20.own;

/**
 * Created by etb on 25.05.16.
 */
public class TestJsons {

    public static final String PLAYLIST = "{\n" +
            "  \"mediaList\": [\n" +
            "    {\n" +
            "      \"filename\": \"http://www.fnordware.com/superpng/pnggrad16rgba.png\",\n" +
            "      \"sequenceNumber\": 0,\n" +
            "      \"duration\": 2000,\n" +
            "      \"id\": 130\n" +
            "    },\n" +
//            "    {\n" +
//            "      \"filename\": \"http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_50mb.mp4\",\n" +
//            "      \"sequenceNumber\": 2,\n" +
//            "      \"duration\": 0,\n" +
//            "      \"id\": 131\n" +
//            "    },\n" +
//            "    {\n" +
//            "      \"filename\": \"http://www.sample-videos.com/video/mp4/720/big_buck_bunny_720p_1mb.mkv\",\n" +
//            "      \"sequenceNumber\": 1,\n" +
//            "      \"duration\": 0,\n" +
//            "      \"id\": 132\n" +
//            "    },\n" +
            "    {\n" +
            "      \"filename\": \"android-tv:80/SampleVideo_360x240_1mb.mp4\",\n" +
            "      \"sequenceNumber\": 2,\n" +
            "      \"duration\": 0,\n" +
            "      \"id\": 131\n" +
            "    }\n" +
            "  ],\n" +
            "  \"name\": \"New Playlist\",\n" +
            "  \"id\": 113\n" +
            "}";
}

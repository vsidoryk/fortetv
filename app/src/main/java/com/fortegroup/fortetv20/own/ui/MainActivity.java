package com.fortegroup.fortetv20.own.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.fortegroup.fortetv20.R;
import com.fortegroup.fortetv20.own.network.DI;
import com.fortegroup.fortetv20.own.MediaControllerListener;
import com.fortegroup.fortetv20.own.PlaybackController;
import com.fortegroup.fortetv20.own.Router;
import com.fortegroup.fortetv20.own.Routing;
import com.fortegroup.fortetv20.own.network.model.DeviceInfo;
import com.fortegroup.fortetv20.own.network.model.Playlist;

import io.vov.vitamio.Vitamio;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;


/**
 * Created by etb on 23.05.16.
 */
public class MainActivity extends Activity implements Routing{

    private Subscription subscription;
    private PlaybackController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Vitamio.isInitialized(this);

        bindFlow();

        setContentView(R.layout.main_activity);
        Router.getInstance(this).loading();

    }

    private void bindFlow() {
        subscription = DI.MODULE.getDeviceInfoService().getObservableValue()
                .flatMap(new Func1<DeviceInfo, Observable<Playlist>>() {
            @Override
            public Observable<Playlist> call(DeviceInfo info) {
                return DI.MODULE.getForteService().getPlaylistObservable();
            }
        }).subscribe(new Action1<Playlist>() {
            @Override
            public void call(Playlist playlist) {
                if (controller != null) {
                    controller.pause();
                }

                if(playlist != null && !playlist.isEmpty()) {
                    controller = new PlaybackController(MainActivity.this, playlist);
                    controller.play();
                } else {
                    Router.getInstance(MainActivity.this).emptyPlaylist();
                }
            }
        });
    }

    public MediaControllerListener getMediaListener(){
        return controller;
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(controller != null){
            controller.pause();
        }

        if(subscription != null){
            subscription.unsubscribe();
        }
    }

    @Override
    public void replaceFragment(Fragment fragment) {
        FragmentTransaction trx = getFragmentManager().beginTransaction();
        trx.replace(R.id.content_frame, fragment);
        trx.commitAllowingStateLoss();
    }
}

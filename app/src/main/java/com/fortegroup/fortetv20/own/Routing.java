package com.fortegroup.fortetv20.own;

import android.app.Fragment;

/**
 * Created by etb on 25.05.16.
 */
public interface Routing {

    public void replaceFragment(Fragment fragment);
}

package com.fortegroup.fortetv20.own;

/**
 * Created by etb on 23.05.16.
 */
public interface MediaControllerListener {

    public void onMediaStop();

    public void onMediaPause();

    public void onMediaPlay();

}

package com.fortegroup.fortetv20.own.ui;

import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fortegroup.fortetv20.R;
import com.fortegroup.fortetv20.own.MediaControllerListener;

import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;


/**
 * Created by etb on 23.05.16.
 */
public class VideoFragment extends Fragment implements MediaControllerObservable, MediaPlayer.OnInfoListener {

    private static final String VIDEO_URL_KEY = VideoFragment.class.getSimpleName() + ".VideoUrl";

    public static VideoFragment newInstance(String videoUrl){
        VideoFragment result = new VideoFragment();
        Bundle args = new Bundle();
        args.putString(VIDEO_URL_KEY, videoUrl);
        result.setArguments(args);

        return result;
    }

    private VideoView video;
    private MediaController controller;

    private MediaControllerListener listener;

    private int position = 0;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video, null, false);
        injectViews(view);
        prepareVideoView();
        restoreInstanceState(savedInstanceState);
        return view;
    }

    private void prepareVideoView(){
//        if (controller == null) {
//            controller = new MediaController(getActivity());
//            controller.setAnchorView(video);
//        }

        video.setVideoURI(getVideoUri());
        video.requestFocus();
        video.setOnInfoListener(this);
        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                // optional need Vitamio 4.0
                mediaPlayer.setPlaybackSpeed(1.0f);
                //video.start();
            }
        });

        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if(listener != null){
                    listener.onMediaStop();
                }
            }
        });


        video.start();


    }

    @Override
    public void onStart() {
        super.onStart();
        injectListener();
    }

    private void injectListener(){
        MainActivity activity = (MainActivity) getActivity();
        setListener(activity.getMediaListener());
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        //savedInstanceState.putInt("Position", video.getCurrentPosition());
        //video.pause();
    }

    public void restoreInstanceState(Bundle savedInstanceState) {
        if(savedInstanceState == null){
            return;
        }

        //position = savedInstanceState.getInt("Position");
        //video.seekTo(position);
    }


    private Uri getVideoUri() {
        return Uri.parse(getArguments().getString(VIDEO_URL_KEY));
    }

    private void injectViews(View view) {
        video = (VideoView)view.findViewById(R.id.videoView);
    }


    @Override
    public void setListener(MediaControllerListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean onInfo(MediaPlayer mp, int what, int extra) {
        switch (what) {
            case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                video.start();
                break;
            case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                //video.start();
                long duration = video.getDuration();
                Log.d("duration", String.format("= %d", duration));
                break;
            case MediaPlayer.MEDIA_INFO_DOWNLOAD_RATE_CHANGED:
                break;
        }
        return true;
    }
}
